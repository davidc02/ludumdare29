﻿using UnityEngine;
using System.Collections;

public class CharacterHealth : MonoBehaviour {

	public int playerHealth;

	void Update() {
		if (playerHealth <= 0) {
			Death();
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		//Lava Collision
		if (coll.gameObject.tag == "Lava") {
			playerHealth = 0;
		}

		//Enemy Collision
		if (coll.gameObject.tag == "Enemy") {
			playerHealth--;
		}

		//Boss Collision
		if (coll.gameObject.tag == "Boss") {
			playerHealth = playerHealth - 2;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "FireBall") {
			playerHealth = playerHealth - 100;
			Destroy(other.gameObject);
		}
	}

	//Player's Death
	void Death() {
		Destroy(gameObject);
	}
}
