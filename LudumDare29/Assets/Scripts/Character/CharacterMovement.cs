﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	public float speed;
	public float jump;

	private bool isGrounded;
	private bool canJump = true;

	public float smoothTime;
	private float yVelocity = 0.0F;

	void Awake () {
		//Disable rotation
		gameObject.rigidbody2D.fixedAngle = true;
	}

	void Update () {
		//Move Right
		if (Input.GetKey (KeyCode.RightArrow)) {
			rigidbody2D.velocity = new Vector2 (Mathf.SmoothDamp(rigidbody2D.velocity.x, speed, ref yVelocity, smoothTime), rigidbody2D.velocity.y);
		}

		//Move Left
		if (Input.GetKey (KeyCode.LeftArrow)) {
			rigidbody2D.velocity = new Vector2 (Mathf.SmoothDamp(rigidbody2D.velocity.x, -speed, ref yVelocity, smoothTime), rigidbody2D.velocity.y);
		}

		//Jump
		if (Input.GetKey (KeyCode.UpArrow) && isGrounded && canJump) {
			//rigidbody2D.AddForce(new Vector2 (0, ));
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jump);
			canJump = false;
		}

		if (Input.GetKeyUp(KeyCode.UpArrow)) {
			canJump = true;
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		//Can only jump while grounded
		if (coll.gameObject.tag == "Ground") {
			isGrounded = true;
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		//Can only jump while grounded
		if (coll.gameObject.tag == "Ground") {
			isGrounded = false;
		}
	}
}
