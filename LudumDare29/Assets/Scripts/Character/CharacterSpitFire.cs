﻿using UnityEngine;
using System.Collections;

public class CharacterSpitFire : MonoBehaviour {

	public GameObject FireParticleSystem;
	//public GameObject CharacterFireTrigger;
	public bool isShooting;

	public AudioClip fireSound;

	void Update () {
		//Shoot Fire
		if (Input.GetKey (KeyCode.Space)) {
			FireParticleSystem.particleSystem.emissionRate = 8000;
			isShooting = true;
			if (!audio.isPlaying) {
				audio.Play();
			}
		}

		//Stop shooting fire
		if (Input.GetKeyUp (KeyCode.Space)) {
			FireParticleSystem.particleSystem.emissionRate = 0;
			isShooting = false;
			audio.Stop();
		}
	}
}
