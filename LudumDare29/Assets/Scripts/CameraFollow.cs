﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public GameObject character;

	public float cameraXoffset;
	public float cameraY;
	public float cameraZ;

	void Update () {
		if (character != null) {
			transform.position = new Vector3(character.transform.position.x + cameraXoffset, cameraY, cameraZ);
		}
	}
}
