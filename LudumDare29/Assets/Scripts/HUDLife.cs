﻿using UnityEngine;
using System.Collections;

public class HUDLife : MonoBehaviour {

	public GameObject character;

	private int playerHealth;

	void Update () {
		if (character != null) {

			playerHealth = character.GetComponent<CharacterHealth>().playerHealth;

			//If Health is Below 0, display 0
			if (playerHealth < 0) {
				gameObject.guiText.text = "0";
			} else {
				//Display Health
				gameObject.guiText.text = "" + playerHealth/10;
			}
		}
	}
}
