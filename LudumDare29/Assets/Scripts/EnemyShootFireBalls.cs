﻿using UnityEngine;
using System.Collections;

public class EnemyShootFireBalls : MonoBehaviour {

	public GameObject fireBall;

	public float initialWaitTime = 2.0f;
	public float waitTime = 2.0f;

	void Awake() {
		InvokeRepeating("ShootFireBall", initialWaitTime, waitTime);
	}

	void ShootFireBall() {
		Instantiate(fireBall, transform.position, Quaternion.identity);
	}
}
