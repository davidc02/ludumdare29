﻿using UnityEngine;
using System.Collections;

public class CharacterAnimation : MonoBehaviour {

	Animator anim;

	void Awake() {
		anim = GetComponent<Animator>();
	}

	void Update() {
		//Jumping
		if (gameObject.transform.parent.rigidbody2D.velocity.y > 0) {
			anim.SetBool("jumping", true);
		} else if (gameObject.transform.parent.rigidbody2D.velocity.y < 0) {
			anim.SetBool("jumping", false);
			anim.SetBool("falling", true);
		} else {
			anim.SetBool("jumping", false);
			anim.SetBool("falling", false);
		}

		//Walking
		if (gameObject.transform.parent.rigidbody2D.velocity.y == 0) {
			if (gameObject.transform.parent.rigidbody2D.velocity.x != 0) {
				anim.SetBool("walk", true);
			} else {
				anim.SetBool("walk", false);
			}
		}

		//Shooting
		if (gameObject.transform.parent.GetComponent<CharacterSpitFire>().isShooting && anim.GetInteger("fire") == 0) {
			anim.SetInteger("fire", 1);
		}

		if (!gameObject.transform.parent.GetComponent<CharacterSpitFire>().isShooting && anim.GetInteger("fire") == 2) {
			anim.SetInteger("fire", 0);
		}
	}

	void setFire2() {
		anim.SetInteger("fire", 2);
	}
}
