﻿using UnityEngine;
using System.Collections;

public class Victory : MonoBehaviour {

	public GameObject character;
	public GameObject victoryScreen;
	public GameObject health;
	public GameObject HUDPentagram;

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			if (character != null) {
				victoryScreen.GetComponent<SpriteRenderer>().color = Color.white;
				health.GetComponent<GUIText>().color = Color.black;
				HUDPentagram.GetComponent<GUITexture>().color = Color.black;
				Destroy(character);
				StartCoroutine("WaitAndMainMenu");
			}
		}
	}

	IEnumerator WaitAndMainMenu() {
		yield return new WaitForSeconds(6);
		Application.LoadLevel("MainMenu");
	}
}