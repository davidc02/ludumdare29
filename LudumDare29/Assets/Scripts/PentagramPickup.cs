﻿using UnityEngine;
using System.Collections;

public class PentagramPickup : MonoBehaviour {

	public GameObject character;

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			if (character != null) {
				character.GetComponent<CharacterHealth>().playerHealth += 100;
				Destroy(gameObject);
			}
		}
	}
}