﻿using UnityEngine;
using System.Collections;

public class TintWhenDamaged : MonoBehaviour {
	private GameObject character;

	void Awake () {
		character = GameObject.Find("Character");
	}

	void Update () {
		if (gameObject.transform.parent.GetComponent<EnemyHealth>().takingDamage == true) {
			if (character != null) {
				gameObject.GetComponent<SpriteRenderer>().color = Color.red;
			} else {
				gameObject.GetComponent<SpriteRenderer>().color = Color.white;
			}
		} else {
			gameObject.GetComponent<SpriteRenderer>().color = Color.white;
		}
	}
}
