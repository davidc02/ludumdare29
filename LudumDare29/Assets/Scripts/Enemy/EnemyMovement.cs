﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	public float speed;

	public bool switchDirection;
	public bool move;

	private float switchDirectionTime;
	public float switchDirectionDelay;

	void Awake () {
		//Disable rotation
		gameObject.rigidbody2D.fixedAngle = true;
	}

	void Update () {
		//Switch Direction
		if ((Time.timeSinceLevelLoad > switchDirectionTime + switchDirectionDelay) && (switchDirection)) {
			speed = speed * -1;
			switchDirectionTime = Time.timeSinceLevelLoad;
		}

		if (move) {
			rigidbody2D.velocity = new Vector2 (speed, rigidbody2D.velocity.y);
		}
	}
}
