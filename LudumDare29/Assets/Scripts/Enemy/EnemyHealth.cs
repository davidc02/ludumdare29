﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	public int health;
	public bool takingDamage;
	private GameObject character;

	void Awake () {
		character = GameObject.Find("Character");
	}


	void Update () {
		if (character != null) {
			//Taking Damage
			if (takingDamage && character.GetComponent<CharacterHealth>().playerHealth > 0) {
				health--;
			}
		}

		//Die if health is 0 or lower
		if (health <= 0) {
			Die();
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		//Lava Collision
		if (coll.gameObject.tag == "Lava") {
			Die();
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.gameObject.tag == "CharacterFire") {
			if (other.transform.parent.gameObject.GetComponent<CharacterSpitFire>().isShooting) {
				takingDamage = true;
			} else {
				takingDamage = false;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.tag == "CharacterFire") {
			takingDamage = false;
		}
	}

	void Die() {
		Destroy(gameObject);
	}
}
