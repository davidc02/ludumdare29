﻿using UnityEngine;
using System.Collections;

public class DeathScreen : MonoBehaviour {

	public GameObject character;
	private int playerHealth;
	private bool canRestart;

	public GameObject health;
	public GameObject HUDPentagram;

	void Update () {
		if (character != null) {
			playerHealth = character.GetComponent<CharacterHealth>().playerHealth;

			//If Player's Health is zero or below, show death screen
			if (playerHealth <= 0) {
				health.GetComponent<GUIText>().color = Color.black;
				HUDPentagram.GetComponent<GUITexture>().color = Color.black;
				gameObject.animation.Play("DeathScreen");
			}
		}

		//Can the Player Restart the game?
		if (canRestart) {
			if (Input.anyKeyDown) {
				Application.LoadLevel("Level");
			}
		}
	}

	//Enable player to restart -- called from DeathScreen Animation
	void enableRestart () {
		canRestart = true;
	}
}
